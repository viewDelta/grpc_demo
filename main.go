package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"example.com/demo/demopb"
	"google.golang.org/grpc"
)

type server struct {
	// Embed the unimplemented server
	demopb.DemoServiceServer
}

func (s *server) Hello(ctx context.Context, request *demopb.DemoRequest) (*demopb.DemoResponse, error) {
	name := request.Name
	log.Printf("Received request : %v\n", name)
	response := &demopb.DemoResponse{
		Greeting: "Hello " + name,
	}
	return response, nil
}

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 9000))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	srv := server{}
	grpcServer := grpc.NewServer()
	demopb.RegisterDemoServiceServer(grpcServer, &srv)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
