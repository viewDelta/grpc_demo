.PHONY: init
init:
	go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go get google.golang.org/protobuf/cmd/protoc-gen-go

.PHONY: generate
generate:
	protoc *.proto --go_out=.
	protoc *.proto --go-grpc_out=.
	go mod tidy